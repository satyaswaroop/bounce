
#import <Foundation/Foundation.h>


#define ISIphone (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define ISIOS7OrGreater [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0
#define loginLangSelectedColor [UIColor colorWithRed:23.0/255.0 green:89.0/255.0 blue:121.0/255.0 alpha:1.0]
#define ISIPHONE5 (([[UIScreen mainScreen] bounds].size.height >= 568) || ([[UIScreen mainScreen] bounds].size.width >= 568))


#define kNavImageNext @"profile.png"
//
#define kColorCodeStatusBar     @"005376"
#define kColorCodeLog           @"b3d0db"
#define kColorCodeLanguage      @"006088"
#define kColorCodetextfont      @"8aa2ac"
#define kColorCodeText          @"000000"
#define kColorcodeViewBG        @"2e2e2e"
#define kColorSegment           @"00ADEF"


#define kColorCodeLblCancelBtn @"0073a0"
#define kColorCodeNav          @"00577b"
#define kColorCodeTopnav       @"70d8ff"
#define kColorCodeNoCount      @"00aeef" //00aeef
#define kColorCodeProfileText  @"ffffff"
#define kColorCodeBgBottomView @"66cef3"






typedef enum
{
    FROMGALLERY,
    FROMCAMERA,
    NONE
} SOURCE;

typedef enum
{
    CONTENT_TYPE_HTML,
    CONTENT_TYPE_JSON
} WS_CONTENT_TYPE;

Boolean Sync;


#define KMsgTitleAlert                     @"Alert"



//messages
#define kMsgError                       @"Unable to load data"
#define kMsgNoNetwork                   @"No Internet Connection"
#define kMsgEnterValidEmail             @"Please enter valid Email"
#define kMsgUnderConstruction           @"Under Construction"
#define kMsgLoginRequired               @"Please login to enable auto sync"
#define kMsgDeleteAdventure             @"Are you sure?"
#define kMsgFieldMandatory              @"All fields are mandatory"
#define kMsgProfileNameMandatory        @"Profile name is mandatory"
#define kPasswordMismatch               @"Re-do password do not match"



#define kApiLogIn                @"http://ndev.checkdots.com/noteUsapi/auth/login"
#define kApiSignUp               @"http://ndev.checkdots.com/noteUsapi/auth/signup"
#define kApiSetProfile           @"http://ndev.checkdots.com/noteUsapi/profile/set_profile"
#define kApiForgetPassword       @"http://ndev.checkdots.com/noteUsapi/auth/set_forget_password"
#define kApiGetFirstTime         @"http://ndev.checkdots.com/noteUsapi/time_screen/get_first_time_screen"
#define kApiGetDiscussion        @"http://ndev.checkdots.com/noteUsapi/discussion/get_discussion"
#define kApiSignUpWithFacebook   @"http://ndev.checkdots.com/noteUsapi/auth/facebook_signup"
//#define kApiSetProfileName       @"http://ndev.checkdots.com/noteUsapi/profile/set_profile"
#define kApiGetTimeLineScreen    @"http://ndev.checkdots.com/noteUsapi/timeline/get_timeline"
#define kApiGetDiscussionScreen  @"http://ndev.checkdots.com/noteUsapi/discussion/get_discussion"
#define kApiGetTimeLine          @"http://ndev.checkdots.com/noteUsapi/timeline/get_timeline"
#define kApiSetDdiscussion       @"http://ndev.checkdots.com/noteUsapi/discussion/set_discussion"


#define kServiceTypeFacebook    @"Facebook"
#define kUDKeyServiceType       @"serviceType"
#define kUDIsUserLoggedIn       @"isUserLoggedIn"
#define kUDKeyUserInfo          @"userInfo"
#define kImageUserProfile       @"userprofile"

#define kUDKeyUserId             @"user_id"
#define kUDKeySetUserId          @"user_id"

#define kUDKeyDiscussionId       @"discussion_id"

#define kUDKeyL_id               @"l_id"
#define kUDKeyP_id               @"p_id"
#define kUDKeyProfile_id         @"profile_id"
#define kUDKeyProfile_name       @"profile_name"
#define kUDKeyProfile_image      @"profile_image"
#define kUDKeyProfile_type       @"profile_type"
#define kUDKeyUpdate_on          @"updated_on"
#define kUDKeyCreated_on         @"created_on"
#define kUDKeyLang_id            @"lang_id"
#define kUDKeyLast_login         @"last_login"
#define kUDKeyLoginStatus        @"LoginStatus"
#define kUDKeyUserProfileImgName @"profile_image"
#define kUDKeyImgExtension       @"png"




#define kUDKeyFirstTimeId       @"first_time_id"        // 1;
#define kUDKeyDiscussionScreen  @"is_discus_screen"     // 1;
#define kUDKeyNoteScreen        @"is_note_screen"       // 0;
#define kUDKeyTimeLineScreen    @"is_timeline_screen"   // 0;



#define kProfilePlaceholderImage  @"profile"

#define kTwitterCunsumerKey  @"2sOMuIEsflxKn66TjyBXl9TDa"
#define kTwitterSecreatKey   @"CWBff9uxD1prDQB2xGCngUJypvDU6NN5ikktmW0bJ2r0ST1u33"

// first tile screen
/*
 {
 status: "success"
 data: {
 first_time_id: "1"
 user_id: "2"
 is_note_screen: "0"
 is_discus_screen: "1"
 is_timeline_screen: "0"
 }-
 }
 
 */


/*
 data =     {
 "created_on" = "2015-07-25 06:30:40";
 email = "abhay.holi@gmail.com";
 "l_id" = 1;
 "lang_id" = 0;
 "last_login" = "2015-08-24 10:53:18";
 "p_id" = 1;
 "profile_id" = 145;
 "profile_image" = "144014875920150821.png";
 "profile_name" = "abhay pratap singh";
 "profile_type" = public;
 status = 1;
 "updated_on" = "2015-07-25 06:30:40";
 "user_id" = 1;
 };
 message = "Login Successful ! ";
 status = success;
 */




/*
 user =     {
 "user_id" = 210;
 }
 message = "Signup successful Please check mail";
 };
 
 */






