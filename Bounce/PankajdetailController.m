//
//  PankajdetailController.m
//  bounce
//
//  Created by Pankaj Singh on 08/06/15.
//  Copyright (c) 2015 wit. All rights reserved.
//

#import "PankajdetailController.h"
#import "GlobleClass.h"
#import "PankajDOBController.h"
#import "EMCCountryPickerController.h"


@interface PankajdetailController ()
{
    GlobleClass *globel;
}
@end

@implementation PankajdetailController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _country.layer.cornerRadius=5.0;
    _country.layer.borderWidth=1.0;
    _next.layer.borderWidth=1.0;
    _next.layer.borderColor=[[UIColor whiteColor] CGColor];
    globel=[GlobleClass getInstance];
    if([UINavigationBar instancesRespondToSelector:@selector(barTintColor)]){ //iOS7
        
        [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],
                                                              NSForegroundColorAttributeName,
                                                              [UIFont boldSystemFontOfSize:18],
                                                              NSFontAttributeName, nil]];
        self.title = @"Information";
    }

    UIButton*menu=[UIButton buttonWithType:UIButtonTypeCustom];
    [menu setImage:[UIImage imageNamed:@"ic_launcher"] forState:UIControlStateNormal];
    [menu addTarget:self action:false forControlEvents:UIControlEventTouchUpInside];
    menu.userInteractionEnabled=false;
    [menu setFrame:CGRectMake(310, 20, 30, 30)];
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithCustomView:menu];
    self.navigationItem.rightBarButtonItem = menuButton;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)Next:(id)sender {
    globel.FirstName=_fname.text;
    globel.lastName=_lname.text;
    globel.emailId=_email.text;
    globel.pass=_passwors.text;
    globel.usercountryName=_country.titleLabel.text;
    
    
    if (globel.FirstName.length==0) {
        UIAlertView * alert=[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Please Enter your First Name." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    
    if (globel.lastName.length==0) {
        UIAlertView * alert=[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Please Enter your Last Name." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    
    if (globel.emailId.length==0) {
        UIAlertView * alert=[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Please Enter your Email Id." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    if ([emailTest evaluateWithObject:globel.emailId] == NO) {
        UIAlertView * alert=[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Not Valid Email Id." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    
    if (globel.pass.length==0) {
        UIAlertView * alert=[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Please Enter Password." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    
    
    if (globel.usercountryName.length==0) {
        UIAlertView * alert=[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Please Select your country." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"dob"];
    [self.navigationController pushViewController:vc animated:YES];

}

- (IBAction)selectCountry:(id)sender {
    
    EMCCountryPickerController * country=[[EMCCountryPickerController alloc] init];
    country.countryDelegate = self;
    [self presentViewController:country animated:YES completion:nil];
    
}

- (void)countryController:(id)sender didSelectCountry:(EMCCountry *)chosenCity
{
    _country.titleLabel.text = chosenCity.countryName;
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{ int movementDistance=0;
    if (textField.tag==2) {
        movementDistance=35;
    }
    if (textField.tag==3) {
        movementDistance = 90;
    }
    const float movementDuration = 0.3f; // tweak as needed
    
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

@end
