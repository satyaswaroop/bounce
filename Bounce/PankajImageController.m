//
//  PankajImageController.m
//  bounce
//
//  Created by Pankaj Singh on 09/06/15.
//  Copyright (c) 2015 wit. All rights reserved.
//

#import "PankajImageController.h"
#import "GlobleClass.h"
#import "PankajServerChanel.h"




@interface PankajImageController ()

@end

@implementation PankajImageController
NSData *imageData;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _registerUser.layer.borderWidth=1.0;
    _registerUser.layer.borderColor=[[UIColor whiteColor] CGColor];
    _uploadImage.layer.cornerRadius=_uploadImage.frame.size.width/2;
    _uploadImage.clipsToBounds=YES;
    if([UINavigationBar instancesRespondToSelector:@selector(barTintColor)]){ //iOS7
        
        [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],
                                                              NSForegroundColorAttributeName,
                                                              [UIFont boldSystemFontOfSize:18],
                                                              NSFontAttributeName, nil]];
        UIButton*menu=[UIButton buttonWithType:UIButtonTypeCustom];
        [menu setImage:[UIImage imageNamed:@"ic_launcher"] forState:UIControlStateNormal];
        [menu addTarget:self action:false forControlEvents:UIControlEventTouchUpInside];
        menu.userInteractionEnabled=false;
        [menu setFrame:CGRectMake(310, 20, 30, 30)];
        UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithCustomView:menu];
        self.navigationItem.rightBarButtonItem = menuButton;
        self.title = @"Image";
    }

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(uploadUserImage:)];
    [_uploadImage addGestureRecognizer: tap];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) uploadUserImage:(UITapGestureRecognizer *)recognizer {
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSLog(@"aaa = %@",info);
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    _uploadImage.image = chosenImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    
}


- (IBAction)userRegister:(id)sender {
    GlobleClass *globle=[GlobleClass getInstance];
    imageData=UIImageJPEGRepresentation(_uploadImage.image, 75);
    
    
    NSMutableDictionary *postData = [[NSMutableDictionary alloc]
                                     initWithObjects:@[@"", @"", @"", @"", @"", @"", @"", @""]
                                     forKeys:@[@"firstname", @"lastname", @"dob", @"gender", @"country", @"email", @"pwd", @"uploaded_file"]];
    [postData setObject:[NSString stringWithFormat:@"%@", globle.FirstName] forKey:@"firstname"];
    [postData setObject:[NSString stringWithFormat:@"%@", globle.lastName] forKey:@"lastname"];
    [postData setObject:[NSString stringWithFormat:@"%@", globle.date] forKey:@"dob"];
    [postData setObject:[NSString stringWithFormat:@"%@", globle.gender] forKey:@"gender"];
    [postData setObject:[NSString stringWithFormat:@"%@", globle.usercountryName] forKey:@"country"];
    [postData setObject:[NSString stringWithFormat:@"%@", globle.emailId] forKey:@"email"];
    [postData setObject:[NSString stringWithFormat:@"%@", globle.pass] forKey:@"pwd"];
    
    
    
    NSString *URL =[NSString stringWithFormat:@"http://bounced.co.uk/hapi/signup"];
    [self.activity startAnimating];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [self submitInformation:URL userinfo:postData  success:^(NSDictionary *jsonDic)
     {
         
         if([[jsonDic objectForKey:@"status"] isEqualToString:@"Error"]){
             [self.activity stopAnimating];
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
             NSString *mesage=[jsonDic objectForKey:@"msg"];
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"%@",mesage] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
             [alert show];

         }else{
             [self.activity stopAnimating];
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
//             LginController * home=[[LginController alloc] init];
//             home.modalTransitionStyle=UIModalTransitionStyleCoverVertical;
//             [self presentViewController:home animated:YES completion:nil];

         }
                      }
                       fail:^(NSError *error)
     {
         [self.activity stopAnimating];
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         
     }];
    
}

-(void)submitInformation:(NSString *)url userinfo:(NSMutableDictionary *)userinfo success: (void (^)(NSDictionary *dict))success fail:(ErrorHandler)failure
{
    //       [self onPost:url];
    
    NSURL *URL = [NSURL URLWithString:url];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"YOUR_BOUNDARY_STRING";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"firstname\"\r\n\r\n%@",[userinfo objectForKey:@"firstname"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"lastname\"\r\n\r\n%@", [userinfo objectForKey:@"lastname"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"dob\"\r\n\r\n%@", [userinfo objectForKey:@"dob"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"gender\"\r\n\r\n%@", [userinfo objectForKey:@"gender"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"country\"\r\n\r\n%@", [userinfo objectForKey:@"country"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"email\"\r\n\r\n%@", [userinfo objectForKey:@"email"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"pwd\"\r\n\r\n%@", [userinfo objectForKey:@"pwd"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"uploaded_file\"; filename=\"Profile.jpg\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [body appendData:[NSData dataWithData:imageData]];
    
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     
     
     {
         
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
         
         // This will Fetch the status code from NSHTTPURLResponse object
         int responseStatusCode = [httpResponse statusCode];
         
         //Just to make sure, it works or not
         NSLog(@"Status Code :: %d", responseStatusCode);
         
         
         
         if(connectionError == nil)
         {
             NSDictionary* result=[NSJSONSerialization JSONObjectWithData:data options:0 error:&connectionError];
             success(result);
         }
         else
         {
             failure(failure);
         }
     }];
    
    
}


@end
