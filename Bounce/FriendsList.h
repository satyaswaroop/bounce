//
//  FriendsList.h
//  b0unced
//
//  Created by Pankaj SIngh on 1/30/15.
//  Copyright (c) 2015 com.webit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendsList : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *friendimage;
@property (weak, nonatomic) IBOutlet UILabel *onlinefriendname;
@property (weak, nonatomic) IBOutlet UIImageView *onleneSignal;

@end
