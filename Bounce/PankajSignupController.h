//
//  PankajSignupController.h
//  bounce
//
//  Created by Pankaj Singh on 07/06/15.
//  Copyright (c) 2015 wit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PankajSignupController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *bmale;
@property (weak, nonatomic) IBOutlet UIButton *bfemale;
@property (weak, nonatomic) IBOutlet UIButton *next;
@property (weak, nonatomic) IBOutlet UIImageView *maleImage;
@property (weak, nonatomic) IBOutlet UIImageView *femaleImage;
- (IBAction)setMale:(id)sender;
- (IBAction)setFemale:(id)sender;
- (IBAction)goNext:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lmale;
@property (weak, nonatomic) IBOutlet UILabel *lfemale;

@end
