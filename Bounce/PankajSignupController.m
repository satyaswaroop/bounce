//
//  PankajSignupController.m
//  bounce
//
//  Created by Pankaj Singh on 07/06/15.
//  Copyright (c) 2015 wit. All rights reserved.
//

#import "PankajSignupController.h"
#import "GlobleClass.h"
#import "PankajdetailController.h"
@interface PankajSignupController ()
{
    GlobleClass *globel;
}
@end

@implementation PankajSignupController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    if([UINavigationBar instancesRespondToSelector:@selector(barTintColor)]){ //iOS7
        self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
        self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
        [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],
                                                              NSForegroundColorAttributeName,
                                                              [UIFont boldSystemFontOfSize:15],
                                                              NSFontAttributeName, nil]];
        UIButton*menu=[UIButton buttonWithType:UIButtonTypeCustom];
        [menu setImage:[UIImage imageNamed:@"ic_launcher"] forState:UIControlStateNormal];
        [menu addTarget:self action:false forControlEvents:UIControlEventTouchUpInside];
        menu.userInteractionEnabled=false;
        [menu setFrame:CGRectMake(310, 20, 30, 30)];
        UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithCustomView:menu];
        self.navigationItem.rightBarButtonItem = menuButton;
        self.title = @"Gender";
    }

    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _bmale.layer.cornerRadius=5.0;
    _bmale.layer.borderWidth=1.0;
     _bmale.layer.borderColor=[[UIColor redColor]CGColor];
    _bfemale.layer.cornerRadius=5.0;
    _bfemale.layer.borderWidth=1.0;
    _bfemale.layer.borderColor=[[UIColor redColor]CGColor];
    _next.layer.borderWidth=1.0;
    _next.layer.borderColor=[[UIColor whiteColor] CGColor];
    globel=[GlobleClass getInstance];
    [_bfemale setBackgroundImage:[UIImage imageNamed:@"uncheck"] forState:UIControlStateNormal];
    [_bmale setBackgroundImage:[UIImage imageNamed:@"uncheck"] forState:UIControlStateNormal];
    }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    [_bfemale setBackgroundImage:[UIImage imageNamed:@"uncheck"] forState:UIControlStateNormal];
    [_bmale setBackgroundImage:[UIImage imageNamed:@"uncheck"] forState:UIControlStateNormal];
    globel.gender=nil;
}

- (IBAction)setMale:(id)sender {
    
    [_bmale setBackgroundImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
    globel.gender=_lmale.text;
    NSLog(@"%@",globel.gender);
    [_bfemale setBackgroundImage:[UIImage imageNamed:@"uncheck"] forState:UIControlStateNormal];

}

- (IBAction)setFemale:(id)sender {
    
    [_bfemale setBackgroundImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
    globel.gender=_lfemale.text;
     NSLog(@"%@",globel.gender);
    [_bmale setBackgroundImage:[UIImage imageNamed:@"uncheck"] forState:UIControlStateNormal];

}

- (IBAction)goNext:(id)sender {
    if (globel.gender==0 || [globel.gender isEqualToString:nil]) {
        UIAlertView * alert=[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Please select gender." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
        
        
    }
    
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"details"];
    [self.navigationController pushViewController:vc animated:YES];
}
@end
