//
//  PankajImageController.h
//  bounce
//
//  Created by Pankaj Singh on 09/06/15.
//  Copyright (c) 2015 wit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PankajImageController : UIViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *uploadImage;
@property (weak, nonatomic) IBOutlet UIButton *registerUser;
- (IBAction)userRegister:(id)sender;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;

@end
