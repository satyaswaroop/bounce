//
//  PankajAppDelegate.h
//  Bounce
//
//  Created by Pankaj Singh on 19/06/15.
//  Copyright (c) 2015 wit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PankajAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
