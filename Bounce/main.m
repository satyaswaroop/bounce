//
//  main.m
//  Bounce
//
//  Created by Pankaj Singh on 19/06/15.
//  Copyright (c) 2015 wit. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PankajAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PankajAppDelegate class]));
    }
}
