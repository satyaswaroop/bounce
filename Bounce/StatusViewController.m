//
//  StatusViewController.m
//  Bounce
//
//  Created by Satya Swaroop Basangi on 14/07/1937 SAKA.
//  Copyright (c) 1937 SAKA wit. All rights reserved.
//

#import "StatusViewController.h"

@interface StatusViewController ()<UITextViewDelegate,UIImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *publicButton;
@property (weak, nonatomic) IBOutlet UIButton *privateButton;
@property (weak, nonatomic) IBOutlet UITextView *statustextView;


@end

@implementation StatusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if([UINavigationBar instancesRespondToSelector:@selector(barTintColor)])
    {   //iOS7
        self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
        self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
        
        [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize:18],NSFontAttributeName, nil]];
        
        self.title = @"Bounced";
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        self.navigationController.navigationBar.translucent = NO;
    }
    
    self.statustextView.text=@"Whats in your mind?";
    self.statustextView.layer.borderWidth = 1.0f;
    self.statustextView.layer.borderColor = [[UIColor grayColor] CGColor];

}



-(IBAction)cameraButtonClicked:(id)sender
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *controllerCamra = [[UIImagePickerController alloc] init];
        [controllerCamra setSourceType:UIImagePickerControllerSourceTypeCamera];
        [self presentViewController:controllerCamra animated:YES completion:Nil];
        
        controllerCamra.delegate=self;
        NSMutableDictionary *mutDict1 = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"Camera",@"gallaryImage", Nil];
        //self.saveDict = mutDict1;
        [self dismissViewControllerAnimated:YES completion:Nil];
    }
    else
    {
        UIAlertView *alertCamera = [[UIAlertView alloc] initWithTitle:@"CAMERA" message:@"Device Not Found. !!!" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        [alertCamera show];
    }

}

-(IBAction)privateOrPublic:(id)sender
{
    
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    textView.text=@"";
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
