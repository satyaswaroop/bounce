//
//  GlobleClass.h
//  b0unced
//
//  Created by Pankaj SIngh on 1/19/15.
//  Copyright (c) 2015 com.webit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GlobleClass : NSObject
+(GlobleClass *)getInstance;

@property(nonatomic,strong)NSString *gender;
@property(nonatomic,strong)NSString *FirstName;
@property(nonatomic, strong)NSString *lastName;
@property(nonatomic, strong)NSString * emailId;
@property(nonatomic, strong)NSString *pass;
@property(nonatomic,strong)NSString *usercountryName;
@property(nonatomic,strong)NSString *date;
@property(nonatomic,strong)NSDictionary *userRecord;
@end
