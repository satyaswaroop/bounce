//
//  PankajDOBController.m
//  bounce
//
//  Created by Pankaj Singh on 09/06/15.
//  Copyright (c) 2015 wit. All rights reserved.
//

#import "PankajDOBController.h"
#import "GlobleClass.h"
#import "PankajImageController.h"


@interface PankajDOBController ()
{
    GlobleClass *globle;
}
@end

@implementation PankajDOBController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _next.layer.borderWidth=1.0;
    _next.layer.borderColor=[[UIColor whiteColor] CGColor];
    globle=[GlobleClass getInstance];
    if([UINavigationBar instancesRespondToSelector:@selector(barTintColor)]){ //iOS7
        
        [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],
                                                              NSForegroundColorAttributeName,
                                                              [UIFont boldSystemFontOfSize:18],
                                                              NSFontAttributeName, nil]];
        UIButton*menu=[UIButton buttonWithType:UIButtonTypeCustom];
        [menu setImage:[UIImage imageNamed:@"ic_launcher"] forState:UIControlStateNormal];
        [menu addTarget:self action:false forControlEvents:UIControlEventTouchUpInside];
        menu.userInteractionEnabled=false;
        [menu setFrame:CGRectMake(310, 20, 30, 30)];
        UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithCustomView:menu];
        self.navigationItem.rightBarButtonItem = menuButton;
        self.title = @"DOB";
    }


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pickerAction:(id)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    
    NSString *formatedDate = [dateFormatter stringFromDate:self.datePicker.date];
    
    self.selectedDate.text =formatedDate;
}

- (IBAction)Next:(id)sender {
    GlobleClass *globel=[GlobleClass getInstance];
    globel.date=_selectedDate.text;
    NSDate *Todaydate=[NSDate date];
    NSDateFormatter *todayformater=[[NSDateFormatter alloc] init];
    [todayformater setDateFormat:@"yyyy/mm/dd"];
    NSString * tdate=[todayformater stringFromDate:Todaydate];
    Todaydate=[todayformater dateFromString:tdate];
    NSDate *userDate=[todayformater dateFromString:self.selectedDate.text];
    if (globel.date.length==0) {
        
        UIAlertView * alert=[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Please select Date of Birth." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    if ([Todaydate compare:userDate]==NSOrderedAscending) {
        
        UIAlertView * alert=[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Invalid Date" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"image"];
    [self.navigationController pushViewController:vc animated:YES];

    
}


@end
