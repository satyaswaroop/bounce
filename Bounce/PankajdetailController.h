//
//  PankajdetailController.h
//  bounce
//
//  Created by Pankaj Singh on 08/06/15.
//  Copyright (c) 2015 wit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EMCCountryDelegate.h"

@interface PankajdetailController : UIViewController<EMCCountryDelegate>

@property (weak, nonatomic) IBOutlet UIButton *next;
- (IBAction)Next:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *fname;
@property (weak, nonatomic) IBOutlet UITextField *lname;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *passwors;
@property (weak, nonatomic) IBOutlet UIButton *country;
- (IBAction)selectCountry:(id)sender;

@end
