//
//  PankajTableViewCell.m
//  Bounce
//
//  Created by Pankaj Singh on 11/07/15.
//  Copyright (c) 2015 wit. All rights reserved.
//

#import "PankajTableViewCell.h"

@implementation PankajTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
