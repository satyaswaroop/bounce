//
//  PankajForgatePasswordController.h
//  bounce
//
//  Created by Pankaj Singh on 14/06/15.
//  Copyright (c) 2015 wit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PankajForgatePasswordController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UIButton *submit;
- (IBAction)doSubmit:(id)sender;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@end
