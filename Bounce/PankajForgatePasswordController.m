//
//  PankajForgatePasswordController.m
//  bounce
//
//  Created by Pankaj Singh on 14/06/15.
//  Copyright (c) 2015 wit. All rights reserved.
//

#import "PankajForgatePasswordController.h"

#import "PankajServerChanel.h"
@interface PankajForgatePasswordController ()

@end

@implementation PankajForgatePasswordController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title=@"Forgate Password";
    }
    return self;
}

- (void)viewDidLoad
{
    if([UINavigationBar instancesRespondToSelector:@selector(barTintColor)]){ //iOS7
        self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
        self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
        [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],
                                                              NSForegroundColorAttributeName,
                                                              [UIFont boldSystemFontOfSize:18],
                                                              NSFontAttributeName, nil]];
        UIButton*menu=[UIButton buttonWithType:UIButtonTypeCustom];
        [menu setImage:[UIImage imageNamed:@"ic_launcher"] forState:UIControlStateNormal];
        [menu addTarget:self action:false forControlEvents:UIControlEventTouchUpInside];
        menu.userInteractionEnabled=false;
        [menu setFrame:CGRectMake(310, 20, 30, 30)];
        UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithCustomView:menu];
        self.navigationItem.rightBarButtonItem = menuButton;
        self.title = @"Forget Password";
    }

    
    

      [super viewDidLoad];
    _submit.layer.borderWidth=1.0;
    _submit.layer.borderColor=[[UIColor whiteColor] CGColor];

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}


- (IBAction)doSubmit:(id)sender {
    NSString *userEmail=_email.text;
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    
    if (userEmail.length==0) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter EmailId. " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }else if ([emailTest evaluateWithObject:userEmail] == NO)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"It is not a Valid Email Address." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }

    NSString *post =[NSString stringWithFormat:@"email=%@",userEmail];
    NSString *URL =[NSString stringWithFormat:@"http://bounced.co.uk/hapi/forgetpass"];
    [self.activity startAnimating];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [[PankajServerChanel sharedInstances]loginUser:URL userpass:post success:^(NSDictionary *dict) {
        [self.activity stopAnimating];
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        if ([[dict objectForKey:@"status"] isEqualToString:@"Success"]) {
             NSString *mesage=[dict objectForKey:@"msg"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information" message:[NSString stringWithFormat:@"%@",mesage] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        NSLog(@"%@",dict);
    } fail:^(NSError *error) {
        [self.activity stopAnimating];
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                NSLog(@"%@",@"sorry");
    } ];
    

}




@end
