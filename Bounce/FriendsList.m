//
//  FriendsList.m
//  b0unced
//
//  Created by Pankaj SIngh on 1/30/15.
//  Copyright (c) 2015 com.webit. All rights reserved.
//

#import "FriendsList.h"

@implementation FriendsList

- (void)awakeFromNib
{
    // Initialization code
    
    
  
    _friendimage.layer.cornerRadius = _friendimage.frame.size.width / 2;
    _friendimage.layer.borderColor=[[UIColor redColor] CGColor];
    _friendimage.layer.borderWidth=1.0;
    _friendimage.clipsToBounds = YES;
    
    _onleneSignal.layer.cornerRadius = _onleneSignal.frame.size.width / 2;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)requestSendYes:(id)sender {
}

- (IBAction)requestSendNo:(id)sender {
}
@end
