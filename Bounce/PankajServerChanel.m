//
//  PankajServerChanel.m
//  b0unced
//
//  Created by Pankaj SIngh on 1/12/15.
//  Copyright (c) 2015 com.webit. All rights reserved.
//

#import "PankajServerChanel.h"

@implementation PankajServerChanel

NSMutableURLRequest *mutablerequest;
NSURLSession *session;

+(id)sharedInstances
{
    static PankajServerChanel *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}


-(void)submitInfoPostMethod:(NSString *)post serverUrl:(NSString *)userUrl
{
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    
    NSString *encodedUrl = [userUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *URL = [NSURL URLWithString:encodedUrl];
    
    mutablerequest = [NSMutableURLRequest requestWithURL:URL];
    [mutablerequest setHTTPMethod:@"POST"];
    [mutablerequest setHTTPBody:postData];
    [mutablerequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [mutablerequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
}

-(void)loginUser:(NSString *)url userpass:(NSString *)userpassDic success:(void (^)(NSDictionary *dict))success fail:(ErrorHandler)failure
{
    [self submitInfoPostMethod:userpassDic serverUrl:url];
    
    [NSURLConnection sendAsynchronousRequest:mutablerequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
         int responseStatusCode = [httpResponse statusCode];
         NSLog(@"Status Code :: %d", responseStatusCode);
         if(connectionError == nil)
         {
             NSDictionary* result=[NSJSONSerialization JSONObjectWithData:data options:0 error:&connectionError];
             success(result);
         }
         else
         {
             failure(failure);
         }
     }];
}

-(void)getUserFeed:(NSString *)url userpass:(NSString *)userpassDic success:(void (^)(NSArray *userFeedArray))success fail:(ErrorHandler)failure;
{

    
    [self submitInfoPostMethod:userpassDic serverUrl:url];
    
    [NSURLConnection sendAsynchronousRequest:mutablerequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
         int responseStatusCode = [httpResponse statusCode];
         NSLog(@"Status Code :: %d", responseStatusCode);
         if(connectionError == nil)
         {
             NSArray* result=[NSJSONSerialization JSONObjectWithData:data options:0 error:&connectionError];
             success(result);
         }
         else
         {
             failure(failure);
         }
     }];

}


@end
