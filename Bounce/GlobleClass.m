//
//  GlobleClass.m
//  b0unced
//
//  Created by Pankaj SIngh on 1/19/15.
//  Copyright (c) 2015 com.webit. All rights reserved.
//

#import "GlobleClass.h"

@implementation GlobleClass
@synthesize gender;
@synthesize FirstName;
@synthesize lastName;
@synthesize emailId;
@synthesize pass;
@synthesize usercountryName;
@synthesize date;
@synthesize userRecord;
static GlobleClass *instance=nil;

+(GlobleClass *)getInstance
{
    @synchronized(self)
    {
        if (instance==nil) {
            instance=[GlobleClass new];
        }
    }
    return instance;
}

@end
