//
//  PankajServerChanel.h
//  b0unced
//
//  Created by Pankaj SIngh on 1/12/15.
//  Copyright (c) 2015 com.webit. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^SuccessHandler)(id responseObject);
typedef void (^ErrorHandler)(NSError *error);

@interface PankajServerChanel : NSObject

+(id)sharedInstances;
-(void)submitInfoPostMethod:(NSString *)post serverUrl:(NSString *)userUrl;
-(void)loginUser:(NSString *)url userpass:(NSString *)userpassDic success:(void (^)(NSDictionary *dict))success fail:(ErrorHandler)failure;
-(void)getUserFeed:(NSString *)url userpass:(NSString *)userpassDic success:(void (^)(NSArray *userFeedArray))success fail:(ErrorHandler)failure;
@end
