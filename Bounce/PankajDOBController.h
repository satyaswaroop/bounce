//
//  PankajDOBController.h
//  bounce
//
//  Created by Pankaj Singh on 09/06/15.
//  Copyright (c) 2015 wit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PankajDOBController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *selectedDate;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIButton *next;
- (IBAction)Next:(id)sender;
- (IBAction)pickerAction:(id)sender;
@end
